FROM maven:3.8.5-openjdk-17 AS builder

# Set working directory
WORKDIR /app

# Copy the project directory
COPY . .

# Install  
RUN mvn clean package

# Stage 2:  
FROM openjdk:17-slim AS runner

# Copy  
COPY --from=builder /app/target/*.jar app.jar

# Expose port 8080 (adjust if needed)
EXPOSE 8080

# Start  
CMD ["java", "-jar", "app.jar"]
